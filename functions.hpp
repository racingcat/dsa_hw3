#pragma once

#include <cmath>

namespace smp{
	template <class T> struct logical_shift_left {
		T operator() (const T& x, const T& y) const { 
			return x<<y; 
		}
	};
	template <class T> struct logical_shift_right {
		T operator() (const T& x, const T& y) const { 
			return x>>y; 
		}
	};
	template <class T> struct logical_and {
		T operator() (const T& x, const T& y) const { 
			return (x!=0 && y!=0); 
		}
	};
	template <class T> struct logical_or {
		T operator() (const T& x, const T& y) const { 
			return (x!=0 || y!=0); 
		}
	};
	template <class T> struct unary_not {
		T operator() (const T& x) const { 
			return (x==0); 
		}
	};
	template <class T> struct unary_plus {
		T operator() (const T& x) const { 
			return x; 
		}
	};
	template <class T> struct unary_minus {
		T operator() (const T& x) const { 
			return x*(-1); 
		}
	};
	
}

namespace sci{
	template <class T> struct sin {
		T operator() (const T& x) const { 
			return std::sin(x); 
		}
	};
	template <class T> struct cos {
		T operator() (const T& x) const { 
			return std::cos(x); 
		}
	};
	template <class T> struct exp {
		T operator() (const T& x) const { 
			return std::exp(x); 
		}
	};
	template <class T> struct sqrt {
		T operator() (const T& x) const { 
			return std::sqrt(x); 
		}
	};
	template <class T> struct fabs {
		T operator() (const T& x) const { 
			return std::fabs(x); 
		}
	};
	template <class T> struct log {
		T operator() (const T& x) const { 
			return std::log(x); 
		}
	};
	template <class T> struct pow {
		T operator() (const T& x, const T& y) const { 
			return std::pow(x,y); 
		}
	};

}
