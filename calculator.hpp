#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <iostream>
#include <map>
#include <string>
#include <iomanip>
#include <stack>
#include <cctype>
#include <functional>
#include "function.hpp"

template<class T>
class Calculator {
public:
	Calculator();
	~Calculator();

	T toNum(std::string::iterator it, std::string::iterator end);
    std::string getOpt(std::string::iterator it, std::string::iterator end){
		if( *it == ')')
			return std::string(")");
		std::string::iterator from = it;
		while(!isdigit(*it) && *it != '(' && !isspace(*it)){
			if( ++it == end )
				return std::string("");
		}
			
		return std::string(from,it-1);
	}
	void evaluate(std::string& expr){
		std::string::iterator it = expr.begin(), end = expr.end();

        //Guard
        optStack.push("\0");

		while(true){
			if(isdigit(*it)){
				T number = toNum(it,end);
				if( number < 0 )
					break;

				std::cout<<number;
				numStack.push(number);
			}
			else{
				auto opt = getOpt(it,end);

				if(opt.empty())
					break;

				auto& fnow = fns[ opt ];
				auto& ftop = fns[ optStack.top() ];
				JudgePrecedence:

					if( ftop < fnow ){
						optStack.push(opt);
					}
					else if( ftop == fnow ){
						optStack.pop();
					}
					else{
						if( !ftop.isSeperator() )
							std::cout<< optStack.top() ;
						optStack.pop();

						if(ftop.isUnary()){
							T n = numStack.top(); numStack.pop(); 
                            numStack.push( static_cast<Unary<T>>(ftop)(n) );
						}
						else if(!ftop.isSeperator()){ // is not something like ( , )
							T n1 = numStack.top(); numStack.pop();
							T n2 = numStack.top(); numStack.pop();
							numStack.push( static_cast<Binary<T>>(ftop)(n1,n2) );
						}
						goto JudgePrecedence;
					}
				
			}
		}
	}
	T getResult(){
		return numStack.top();
	}

private:
	std::stack<T> numStack;
	std::stack<std::string> optStack;
protected:
	std::map<std::string,Function> fns;
};


#endif
