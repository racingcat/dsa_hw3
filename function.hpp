#ifndef FUNCTION_H
#define FUNCTION_H
#include <functional>

class Function {
public:
	
	bool operator < (const Function& rhs) const{
		return precedence < rhs.precedence;
	}
	bool operator == (const Function& rhs) const{
		return precedence == rhs.precedence;
	}
	virtual bool isSeperator(){
		return false;
	}

	bool isUnary();

protected:
	int precedence;
};

class Seperator: public Function {
public:
	Seperator(int p){
        precedence = p;
    }
	bool isSeperator(){
		return true;
	}
};

template<class T>
class Unary: public Function {
public:
	Unary(std::function<T(const T)> f, int p){
        func = f;
        precedence = p;
    }
	T operator()(const T& x){
		return func(x);
	}
	bool isUnary(){
		return true;
	}
private:
	std::function<T(const T)> func;
};

template<class T>
class Binary: public Function {
public:
	Binary(std::function<T(const T,const T)> f, int p){
        func = f;
        precedence = p;
    }
	T operator()(const T& x,const T& y){
		return func(x,y);
	}
	bool isUnary(){
		return false;
	}

private:
	std::function<T(const T,const T)> func;
};

#endif
