#ifndef SIMPLE_H
#define SIMPLE_H

#include "function.hpp"
#include "functions.hpp"
#include "calculator.hpp"

template<class Integer>
class Simple: public Calculator<Integer> {
public:
	Simple(){
		
		int precedence = 100;
		this->fns.emplace( "!" , Unary<Integer>(  smp :: unary_not<Integer>()           , --precedence));
		this->fns.emplace( "-" , Unary<Integer>(  smp :: unary_minus<Integer>()         , --precedence));
		this->fns.emplace( "+" , Unary<Integer>(  smp :: unary_plus<Integer>()          , --precedence));
		this->fns.emplace( "*" , Binary<Integer>( std :: multiplies<Integer>()          , --precedence));
		this->fns.emplace( "/" , Binary<Integer>( std :: divides<Integer>()             , --precedence));
		this->fns.emplace( "%" , Binary<Integer>( std :: modulus<Integer>()             , --precedence));
		this->fns.emplace( "+" , Binary<Integer>( std :: plus<Integer>()                , --precedence));
		this->fns.emplace( "-" , Binary<Integer>( std :: minus<Integer>()               , --precedence));
		this->fns.emplace( "<<", Binary<Integer>( smp :: logical_shift_left<Integer>()  , --precedence));
		this->fns.emplace( ">>", Binary<Integer>( smp :: logical_shift_right<Integer>() , --precedence));
		this->fns.emplace( "&" , Binary<Integer>( std :: bit_and<Integer>()             , --precedence));
		this->fns.emplace( "^" , Binary<Integer>( std :: bit_xor<Integer>()             , --precedence));
		this->fns.emplace( "|" , Binary<Integer>( std :: bit_or<Integer>()              , --precedence));
		this->fns.emplace( "&&", Binary<Integer>( smp :: logical_and<Integer>()         , --precedence));
		this->fns.emplace( "||", Binary<Integer>( smp :: logical_or<Integer>()          , --precedence));

		/* Only Parentheses are of same precedence */
		this->fns.emplace( "(", Seperator( precedence ));
		this->fns.emplace( ")", Seperator( precedence ));

	}

	Integer toNum(std::string::iterator& it,std::string::iterator& end){
		Integer num = 0;
		while(isdigit(*it)){
			num = num*10 + *it - '0';
			if( ++it == end )
				return -1;
		}
		return num;		
	}

	

};


#endif
