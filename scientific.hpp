#ifndef SCIENTIFIC_H
#define SCIENTIFIC_H

#include "function.hpp"
#include "functions.hpp"
#include "calculator.hpp"

template<class Float>
class Scientific: public Calculator<Float> {
public:
	Scientific(){
		int precedence = 100;
		this->fns.emplace(  "-"    , Unary<Float>(  smp ::unary_minus<Float>() , --precedence ));
		this->fns.emplace(  "+"    , Unary<Float>(  smp ::unary_plus<Float>()  , --precedence ));
		this->fns.emplace(  "sin"  , Unary<Float>(  sci ::sin<Float>()         , --precedence ));
		this->fns.emplace(  "cos"  , Unary<Float>(  sci ::cos<Float>()         , --precedence ));
		this->fns.emplace(  "exp"  , Unary<Float>(  sci ::exp<Float>()         , --precedence ));
		this->fns.emplace(  "sqrt" , Unary<Float>(  sci ::sqrt<Float>()        , --precedence ));
		this->fns.emplace(  "fabs" , Unary<Float>(  sci ::fabs<Float>()        , --precedence ));
		this->fns.emplace(  "sqrt" , Unary<Float>(  sci ::sqrt<Float>()        , --precedence ));
		this->fns.emplace(  "log"  , Unary<Float>(  sci ::log<Float>()         , --precedence ));
		this->fns.emplace(  "/"    , Binary<Float>( std ::divides<Float>()     , --precedence ));
		this->fns.emplace(  "*"    , Binary<Float>( std ::multiplies<Float>()  , --precedence ));
		this->fns.emplace(  "+"    , Binary<Float>( std ::plus<Float>()        , --precedence ));
		this->fns.emplace(  "-"    , Binary<Float>( std ::minus<Float>()       , --precedence ));
		this->fns.emplace(  "pow"  , Binary<Float>( sci ::pow<Float>()         , --precedence ));
		
		this->fns.emplace( ",", Seperator( --precedence ));
		/* Only Parentheses are of same precedence */
		this->fns.emplace( "(", Seperator(   precedence ));
		this->fns.emplace( ")", Seperator(   precedence ));
	
		

	}

	Float toNum(std::string::iterator& it, std::string::iterator& end){
		Float num = 0.0;
		while(true){
			if(*it == '.')
				break;
			else if(isdigit(*it))
				num = num*10.0 + *it - '0';
			else
				return num;

			if( ++it == end )
				return -1.0;
		}

		/* After decimal point */
		Float scalar = 0.1;
		while(isdigit(*it)){
			num = num + (*it - '0')*scalar;
			scalar *= 0.1;
			if( ++it == end )
				return -1.0;
		}
		return num;	
	}

};


#endif
