#include<iostream>
#include<string>

// #ifdef SCIENTIFIC
// #include "scientific.hpp"
// #else 
// #include "simple.hpp"
// #endif
#include "scientific.hpp"
#include "simple.hpp"

int main(int argc, char const *argv[])
{
#ifdef SCIENTIFIC
	Scientific<int> calculator;
#else
	Simple<int> calculator;
#endif
	std::string expr;
	while(std::getline(std::cin,expr)){
		std::cout<<"Postfix Exp: ";
		calculator.evaluate(expr);
		std::cout<<"\n";

		std::cout<<"RESULT: "<<calculator.getResult()<<"\n";
	}
	return 0;
}
