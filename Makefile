SRC=$(wildcard *.cpp)

DIR_SCI := bin/sci
DIR_SMP := bin/smp
OBJECTS_SCI:= $(patsubst %.cpp, $(DIR_SCI)/%.o, ${SRC})
OBJECTS_SMP:= $(patsubst %.cpp, $(DIR_SMP)/%.o, ${SRC})
CPPFLAGS=-g -std=c++11 -Wall
CC=clang

all: 
	@echo $(SRC)
	@echo $(OBJECTS_SCI)

scientific:  $(OBJECTS_SCI) 
	@echo "[linking]"$@	
	$(CC) -o $@ $^

simple: $(OBJECTS_SMP)
	@echo "[linking]"$@	
	$(CC) -o $@ $^

$(DIR_SCI)/%.o: %.cpp
	@echo "[compiling]"$@	
	$(CC) $(CPPFLAGS) -DSCIENTIFIC -c $< -o $@ 

$(DIR_SMP)/%.o: %.cpp
	@echo "[compiling]"$@	
	$(CC) $(CPPFLAGS) -DSIMPLE -c $< -o $@ 

